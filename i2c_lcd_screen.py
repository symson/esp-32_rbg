#!/usr/bin/python
# -*- coding: utf-8 -*-
# This is a port of https://github.com/Seeed-Studio/Grove_LCD_RGB_Backlight
# (c) 2017 Alex Bucknall <alex.bucknall@gmail.com>

from machine import I2C
import time

class Screen(object):
    # commands
    DISP_CMD       = b'0x00' #Command for the display
    RAM_WRITE_CMD  = b'0x40' #Write to display RAM
    CLEAR_DISP_CMD = b'0x01' #Clear display command
    HOME_CMD       = b'0x02' #Set cursos at home (0,0)
    DISP_ON_CMD    = b'0x0C' #Display on command
    DISP_OFF_CMD   = b'0x08' #Display off Command
    SET_DDRAM_CMD  = b'0x80' #Set DDRAM address command
    CONTRAST_CMD   = b'0x70' #Set contrast LCD command
    FUNC_SET_TBL0  = b'0x38' #Function set - 8 bit, 2 line display 5x8, inst table 0
    FUNC_SET_TBL1  = b'0x39' #Function set - 8 bit, 2 line display 5x8, inst table 1
    CMD_DELAY           = 1 # Command delay in miliseconds
    CHAR_DELAY          = 0 # Delay between characters in miliseconds
    PIXEL_ROWS_PER_CHAR = 8 # Number of pixel rows in the LCD character
    MAX_USER_CHARS = 16     # Maximun number of user defined characters

    dram_dispAddr = [ [ 0x00, 0x00, 0x00 ], [ 0x00, 0x40, 0x00 ], [ 0x00, 0x10, 0x20 ] ]


    CURSOR_ON_BIT  = ( 1 << 1 ) #Cursor selection bit in Display on cmd.
    BLINK_ON_BIT   = ( 1 << 0 ) #Blink selection bit on Display on cmd.


    def __init__(self, i2c, address, num_lines=2, num_col=16, backlightPin=-1):
        self.num_lines    = num_lines
        self.num_col      = num_col
        self.cmdDelay     = self.CMD_DELAY
        self.charDelay    = self.CHAR_DELAY
        self.backlightPin = backlightPin
        self.i2c = i2c
        self.address = address
        self.disp_ctrl = self.DISP_ON_CMD



        time.sleep_ms(50) # 50ms
        self.i2c.writeto(self.address, b'0x00')
        self.i2c.writeto(self.address, self.FUNC_SET_TBL0)
        time.sleep_ms(10) # 10ms
        self.i2c.writeto(self.address, self.FUNC_SET_TBL1)
        time.sleep_ms(10) # 10ms
        self.i2c.writeto(self.address, b'0x14' ) # // Set BIAS - 1/5
        self.i2c.writeto(self.address, b'0x78' ) # // Set contrast low byte
        self.i2c.writeto(self.address, b'0x5E' ) # // ICON disp on, Booster on, Contrast high byte
        self.i2c.writeto(self.address, b'0x6D' ) # // Follower circuit (internal), amp ratio (6)
        self.i2c.writeto(self.address, b'0x0C' ) # // Display on
        self.i2c.writeto(self.address, b'0x01' ) # // Clear display
        self.i2c.writeto(self.address, b'0x06' ) # // Entry mode set - increment



    def cmd(self, command):
        self.i2c.writeto(self.address, self.DISP_CMD)
        self.i2c.writeto(self.address, command)
        time.sleep_ms(self.cmdDelay)


    def write_char(self, c):
        if(c == b'\n'):
            self.setCursor (1,0)
        else:
            c = bytearray([c])
            self.i2c.writeto(self.address, self.RAM_WRITE_CMD)
            self.i2c.writeto(self.address, c)
            time.sleep_ms(self.charDelay)

    def write(self, text):
        for char in text:
            self.write_char(ord(char))

    def cursor(self, state):
        if state:
            self.disp_ctrl = self.disp_ctrl | self.CURSOR_ON_BIT
            self.cmd(self.DISP_ON_CMD | self.CURSOR_ON_BIT)
        else:
            self.disp_ctrl = self.disp_ctrl & ~self.CURSOR_ON_BIT
            self.cmd(self.DISP_ON_CMD | self.CURSOR_ON_BIT)

    def setCursor(self, col, row):
        base = 0
        base = dram_dispAddr[self.num_lines-1][self.row]
        base = self.SET_DDRAM_CMD + base + row
        self.cmd(base)

    def blink(self, state):
        if state:
            self.disp_ctrl = self.disp_ctrl | self.BLINK_ON_BIT
            self.cmd(self.DISP_ON_CMD | self.disp_ctrl)
        else:
            self.disp_ctrl = self.disp_ctrl & ~self.BLINK_ON_BIT
            self.cmd(self.DISP_ON_CMD  | self.disp_ctrl)

    def display(self, state):
        if state:
            self.disp_ctrl = self.disp_ctrl | self.DISP_ON_CMD
            self.cmd(self.DISP_ON_CMD | self.disp_ctrl)
        else:
            self.disp_ctrl = self.disp_ctrl | ~self.DISP_ON_CMD
            self.cmd(self.DISP_ON_CMD | self.disp_ctrl)

    def clear(self):
        self.cmd(self.CLEAR_DISP_CMD)

    def home(self):
        self.cmd(self.HOME_CMD)
