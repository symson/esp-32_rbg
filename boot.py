import esp
import network
import webrepl
import ntptime
import machine
import time
import i2c_lcd
import neopixel
import utime

def connect():
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect('MonteCristo', 'edmond et trouline sont morts')
        while not sta_if.isconnected():
            pass
    print('network config:', sta_if.ifconfig())


def scan():
    i2c=machine.I2C(scl=machine.Pin(22,machine.Pin.OUT, machine.Pin.PULL_UP), sda=machine.Pin(21,machine.Pin.OUT, machine.Pin.PULL_UP),freq=100000)
    list_device=i2c.scan()
    return i2c, list_device

def demo(np, delay=25):
    n = np.n
    # fade in/out
    while True:
        for i in range(0, 4 * 256, 8):
            for j in range(n):
                if (i // 256) % 2 == 0:
                    val = i & 0xff
                else:
                    val = 255 - (i & 0xff)
                np[j] = (val, val, val)
            np.write()
            time.sleep_ms(delay)

def clear(np):
    n = np.n
    # fade in/out
    for i in range(n):
        np[i] = (0, 0, 0)

    np.write()


def transition(np, start= { 'red' : 1, 'green' : 1, 'blue' : 1}, end = { 'red' : 249, 'green' : 253, 'blue' : 75}, total_delay=5):
    gap = {}
    gap['red']   = end ['red' ] - start[ 'red' ]
    gap['green'] = end ['green' ] - start[ 'green' ]
    gap['blue']  = end ['blue' ] - start[ 'blue' ]
    max_gap_color = max(gap, key=gap.get)
    step_duration = int((total_delay * 1000) / gap[max_gap_color])
    step = {}
    step[max_gap_color] = 1
    color_remaining = [ 'red', 'green', 'blue']
    color_remaining.remove(max_gap_color)
    for color in color_remaining:
        step[color] = gap[color] / gap[max_gap_color]

    val = start.copy()
    for i in range(gap[max_gap_color]):
        val['red']   = val['red']   + step['red']
        val['green'] = val['green'] + step['green']
        val['blue']  = val['blue']  + step['blue']

        for j in range(np.n):
            np[j] = (int(val['red']), int(val['green']), int(val['blue']))
        np.write()
        time.sleep_ms(step_duration)


def reveil(hour, minute):
    cur_time = utime.localtime()
    print("il est {}:{}".format(cur_time[3], cur_time[4]))
    if(cur_time[3] == hour and cur_time[4] == minute):
            print("Que la lumière soit\n")
            transition(np, start= { 'red' : 0, 'green' : 0, 'blue' : 0}, end = { 'red' : 255, 'green' : 255, 'blue' : 255}, total_delay=600)
    else:
        print("Go dodo")
        time.sleep_ms(3000)
        machine.deepsleep(27000)



connect()
esp.osdebug(None)
webrepl.start()
while True:
    try:
        ntptime.settime()
    except:
        continue
    break
#i2c, list_device = scan()
#if(len(list_device) == 2):
#    d = i2c_lcd.Display(i2c, lcd_addr=list_device[1])
#    d.home()
#    d.write('Coucou')
#
#
np = neopixel.NeoPixel(machine.Pin(15), 24)
clear(np)
#print("GO rEVEIL")
#reveil(21,02)
#print("GONE rEVEIL")
#transition(np, start= { 'red' : 0, 'green' : 0, 'blue' : 0}, end = { 'red' : 255, 'green' : 255, 'blue' : 255}, total_delay=6)

